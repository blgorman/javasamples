package demonstrations;

import java.util.Scanner;

import CustomObjects.Car;

public class JavaPassByReferenceValueDemo implements DemonstrationExecutor {
	Scanner input;
	public JavaPassByReferenceValueDemo(Scanner in)
	{
		input = in;
	}
	
	@Override
	public void Execute() {
		System.out.println("Java Pass By Reference Value Demo");
		System.out.println("Is Java 'Pass-By-Reference' or 'Pass-By-Value'?");
		//complex objects have pointers which are "referenced" when passing
		//this is why we need cloning in specific instances.
		//Even though the pointer is Pass-by-value, 
		//the memory address of an object will still affect the objects:
		System.out.println("Create two cars with different pointers: ");
		Car car1 = new Car("Ford", "Mustang", 1967, "Blue");
		System.out.printf("Car1] %s\n", car1);
		Car car2 = pseudoCloneCar(car1);
		System.out.printf("Car2] %s\n", car2);
		
		System.out.println("Create a new car with pointer to car 1 "
							+ "[note that you will affect BOTH when changing properties now]:");
		Car car3 = copyCar(car1);
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);
		
		System.out.println("Change car 1 = also changes car 3");
		changeCar(car1);
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);
		
		System.out.println("Create array with all three cars, modify them [Year, Make, Model on car 1/3, color on car2]");
		Car[] myCars = new Car[] {car1, car2, car3};
		modifyCars(myCars);
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);
		
		//but wait..
		System.out.println("If variables were passed by reference, you would be able to change the car here...");
		changeEntireCar(car2);
		System.out.println("Yet, you see it did not change:");
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);
		
		//no reference:
		System.out.println("And the primitives passed are also ignored...");
		changeColor(car2.getColor());
		changeYear(car3.getYear());
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);

		System.out.println("But, if we pass the pointer to the object, we CAN modify the object properties...");
		actuallyChangeColor(car2);
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);
		
		System.out.println("Note that again it will affect BOTH cars referenced because they are both pointing to the same object in memory...");
		actuallyChangeYear(car1);
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);
		
		System.out.println("Finally, you would think [by ref] that passing the ARRAY would allow for changing...but it does not....");
		tryChangeEntireCar(myCars);
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);
		
		anotherTryChangeEntireCar(myCars);
		System.out.printf("Car1] %s\n", car1);
		System.out.printf("Car2] %s\n", car2);
		System.out.printf("Car3] %s\n", car3);
		System.out.println();
		System.out.println("So we can see changing the pointer inside of a method clearly "
							+ "has no effect on the pointer outside of the method \n"
							+ "which means ALL values are actually passed byValue "
							+ "but as expected we CAN change existing properties as long as "
							+ "we don't create new objects.");
	}

	public Car copyCar(Car theCar)
	{
		Car car = theCar;
		car.setYear(2005);
		car.setMake("Chevy");
		car.setModel("Camaro");
		car.setColor("Red");
		return car;
	}
	
	public Car pseudoCloneCar(Car c)
	{
		Car cloneCar = new Car();
		cloneCar.setYear(c.getYear());
		cloneCar.setColor(c.getColor());
		cloneCar.setMake(c.getMake());
		cloneCar.setModel(c.getModel());
		return cloneCar;
	}
	
	public void changeCar(Car c)
	{
		c.setMake("Dodge");
		c.setModel("Charger");
		c.setYear(1969);
		c.setColor("Orange");
	}
	
	public void modifyCars(Car[] theCars)
	{
		theCars[0].setYear(2000);
		theCars[1].setColor("Black");
		theCars[2].setMake("Nissan");
		theCars[2].setModel("Altima");
	}
	
	public void changeColor(String color)
	{
		color = "Green";
	}
	
	public void actuallyChangeColor(Car c)
	{
		c.setColor("Green");
	}
	
	public void changeYear(int year)
	{
		year = 2001;
	}
	
	public void actuallyChangeYear(Car c)
	{
		c.setYear(2011);
	}
	
	public void changeEntireCar(Car c)
	{
		c = new Car("Honda", "CRV", 2008, "Yellow");
		System.out.printf("Car changed to: %s\n", c);
	}
	
	public void tryChangeEntireCar(Car[] cars)
	{
		cars[1] = new Car("Toyota", "Tundra", 2007, "White");
	}
	
	public void anotherTryChangeEntireCar(Car[] cars)
	{
		Car c = new Car("Toyota", "Tundra", 2007, "White");
		cars = new Car[] {cars[0], c, cars[1]};
		for (Car car : cars)
		{
			System.out.printf("Car] %s\n", car);
		}
		System.out.println("");
	}
}
