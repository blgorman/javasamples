package demonstrations;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SubtractingDatesDemo implements DemonstrationExecutor {
	Scanner input;
	public SubtractingDatesDemo(Scanner in)
	{
		input = in;
	}
	
	@Override
	public void Execute() {
		System.out.println("Subtracting Dates Demonstration");
		System.out.println("Why does subtracting two dates give a strange result?");
		dashes(80);
		
		//There are two ways to look at dates -
		//1) A distinct time in space (i.e. Let's meet on December 1 at 12:00 noon for lunch)
		//2) A duration between two points in time (i.e. We need to reboot the server every N days, without
		//		an overlap of even one nanosecond).
		//Depending on how you want to interact with the datetime and your current timezones
		//     coupled with the possibility of a Time Discontinuity, you need to handle it appropriately in code
		//This problem was originally caused by a time discontinuity of 5 minutes, but is no longer able to be
		//		reproduced in code for the same date, it is now between 1900 and 1901
		//		and running the calculations doesn't show me the result anymore (may have something to do with 
		//      utc offset for 1900 and timezones)
		//However, there are still time discontinuities that can be shown using OLD java datetime code
		try
		{
			showOriginalDiscontinuity();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		dashes(80);
		
		//FIRST - let's just show a common discontinuity for DST in Chicago and Berlin
		showChicagoDiscontinuity();
		dashes(80);
		showBerlinDiscontinuity();
		dashes(80);

		//so let's make sure we know exactly which we are using
		//and let's always do this in java 8 with the new time api
		//if you must do this in < java 8, look into JODATIME
		
		doItRightChicago();
		dashes(80);
		doItRightShanghai();
		dashes(80);
		doItRightBerlin();
		dashes(80);
	}
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	private final int MILLIS_IN_HOUR = 1000 * 60  * 60;
	private final int MILLIS_IN_DAY = MILLIS_IN_HOUR * 24;
	
	public void showOriginalDiscontinuity() throws ParseException
	{
		System.out.println("SHANGHAI DATE PROBLEM");
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
		//String d1 = "1927-12-31 23:54:07";
		//String d2 = "1927-12-31 23:54:08";
		
		String d1 = "1900-12-31 23:59:00";
		String d2 = "1901-01-01 00:00:00";
		
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		Date dt1 = sf.parse(d1);
		Date dt2 = sf.parse(d2);
		System.out.println(dt1.toString());
		System.out.println(dt2.toString());
		long dt1LongVal = dt1.getTime() / 60 / 1000;
		long dt2LongVal = dt2.getTime() / 60 / 1000;
		System.out.println(dt1LongVal);
		System.out.println(dt2LongVal);
		long diff = (dt2LongVal - dt1LongVal);
		System.out.println(diff);
	}
	
	public void showChicagoDiscontinuity()
	{
		System.out.println("CHICAGO: 3/12-3/13/2016");
		TimeZone.setDefault(TimeZone.getTimeZone("America/Chicago"));
		
		Date march12 = new Date("03/12/2016 12:00:00");
		Date march13 = new Date("03/13/2016 12:00:00");
		
	    Calendar m12 = new GregorianCalendar(2016, 2, 12, 12, 0, 0);
	    Calendar m13 = new GregorianCalendar(2016, 2, 13, 12, 0, 0);
	    
	    calculationOne(march12, march13, true);
	    calculationTwo(m12, m13);
	    
	    dashes(20);
	    System.out.println("BERLIN: 3/12-3/13/2016");
	    TimeZone.setDefault(TimeZone.getTimeZone("Europe/Berlin"));
	    Date march12_new = new Date("03/12/2016 12:00:00");
		Date march13_new = new Date("03/13/2016 12:00:00");
	    calculationOne(march12_new, march13_new);
	}
	
	public void showBerlinDiscontinuity()
	{
		System.out.println("BERLIN: 3/26-3/27/2016");
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Berlin"));
		Date march26 = new Date("03/26/2016 12:00:00");
		Date march27 = new Date("03/27/2016 12:00:00");
		
	    Calendar m26 = new GregorianCalendar(2016, 2, 26, 12, 0, 0);
	    Calendar m27 = new GregorianCalendar(2016, 2, 27, 12, 0, 0);
	    
	    calculationOne(march26, march27);
	    calculationTwo(m26, m27);
	    dashes(20);
	    
	    System.out.println("CHICAGO: 3/26-3/27/2016");
	    TimeZone.setDefault(TimeZone.getTimeZone("America/Chicago"));
		Date march26_new = new Date("03/26/2016 12:00:00");
		Date march27_new = new Date("03/27/2016 12:00:00");
	    calculationOne(march26_new, march27_new);
	}
	
	private void calculationOne(Date start, Date end)
	{
		calculationOne(start, end, false);
	}
	
	private void calculationOne(Date start, Date end, boolean showDiff)
	{
		long diff = end.getTime() - start.getTime();
		if (showDiff)
		{
			System.out.println("Actual difference " + diff);
			System.out.println("Number of millis in day: " + MILLIS_IN_DAY);
			System.out.println(MILLIS_IN_DAY - diff);
			System.out.println(1000 * 60 * 60);
		}
		long days = diff/MILLIS_IN_DAY;
		System.out.printf("Using Dates - days diff: %d\n"
							, days); 
	}
	
	private void calculationTwo(Calendar start, Calendar end)
	{
		Calendar date = (Calendar) start.clone();
		long daysBetween = 0;
		while (date.before(end)) {
		    date.add(Calendar.DAY_OF_MONTH, 1);
		    daysBetween++;
		}
		System.out.printf("Using Calendar days - diff: %d\n"
							, daysBetween);
	}

	private void doItRightChicago()
	{
		Locale.setDefault(Locale.US);
		TimeZone.setDefault(TimeZone.getTimeZone("America/Chicago"));
		
		commonDoItRight();
	}
	
	private void doItRightBerlin()
	{
		Locale.setDefault(Locale.GERMANY);
		TimeZone.setDefault(TimeZone.getTimeZone("Europe/Berlin"));
		
		commonDoItRight();
	}
	
	private void doItRightShanghai()
	{
		Locale.setDefault(Locale.CHINA);
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
		
		commonDoItRight();
	}
	
	public static void commonDoItRight()
	{
		LocalDateTime start = LocalDateTime.of(2016, Month.MARCH, 1, 12, 0, 0);
		LocalDateTime end = LocalDateTime.of(2016, Month.JULY, 1, 12, 0, 0);
		
		doItRight(start, end);
	}
	
	public static void doItRight(LocalDateTime start, LocalDateTime end)
	{
		long days = Duration.between(start, end).toDays();
		long hours = Duration.between(start, end).toHours();
		long minutes = Duration.between(start, end).toMinutes();
		long millis = Duration.between(start, end).toMillis();
		long nanos = Duration.between(start, end).toNanos();
		
		System.out.printf("d: %d h: %d m: %d millis: %d  nanos: %d\n"
							, days
							, hours
							, minutes
							, millis
							, nanos);
		System.out.printf("Difference in Nanos is constant, what matters is if you want to handle t/z or not? %d nanos\n"
							, nanos);
		System.out.printf("Add %d nanos to %s:\n"
							, nanos
							, start.toString());
		calculateResults(start, nanos);
		System.out.printf("Subtract %d nanos from %s:\n"
				, nanos
				, end.toString());
		calculateResults(end, nanos * -1);
	}
	
	public static void calculateResults(LocalDateTime start, long nanos)
	{
		LocalDateTime result1 = start.plusNanos(nanos);
		ZonedDateTime zdt = ZonedDateTime.of(start, TimeZone.getDefault().toZoneId());
		ZonedDateTime result2 = zdt.plusNanos(nanos);
		
		System.out.println("Ignoring Time Discontinuity: " + result1.toString());
		System.out.println("Handling Time Discontinuity: " + result2.toString());
	}
	
	private void dashes(int length)
	{
		System.out.println(Stream.generate(
				() -> String.valueOf('-'))
				.limit(length)
				.collect(Collectors.joining()));
	}
}
