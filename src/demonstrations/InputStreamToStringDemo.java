package demonstrations;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Scanner;


public class InputStreamToStringDemo implements DemonstrationExecutor {
	Scanner input;
	final int BUFFER_SIZE = 1024;
	
	public InputStreamToStringDemo(Scanner in)
	{
		input = in;
	}
	
	@Override
	public void Execute() {
		System.out.println("Input Stream To String Demo");
		System.out.println("How do I read and convert an InputStream to a String?");
		
		FileInputStream fis = null;
		FileInputStream fis2 = null;
		//open the file:
		try
		{
			fis = new FileInputStream("C:\\Data\\InputStreamToString.txt");
			fis2 = new FileInputStream("C:\\Data\\InputStreamToString.txt");
			if (fis != null)
			{
				ConvertInputStreamToStringByteArrayOutputStream(fis);
				ConvertInputStreamToStringWithStringBuilder(fis2);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.out);
			return;
		}
		finally
		{
			if (fis != null)
			{
				try
				{
					fis.close();
				} 
				catch (IOException e) 
				{
					//nothing to do here
				}
			}
			if (fis2 != null)
			{
				try
				{
					fis2.close();
				} 
				catch (IOException e) 
				{
					//nothing to do here
				}
			}
		}
		
	}
	
	private void ConvertInputStreamToStringByteArrayOutputStream(InputStream is) throws IOException
	{
		ByteArrayOutputStream barOutStream = new ByteArrayOutputStream();
		byte[] buf = new byte[BUFFER_SIZE];
		int length;
		
		while((length = is.read(buf)) != -1)
		{
			barOutStream.write(buf, 0, length);
		}
		
		System.out.println(barOutStream.toString());
	}

	private void ConvertInputStreamToStringWithStringBuilder(InputStream is) throws IOException
	{
		char[] buf = new char[BUFFER_SIZE];
		Reader in = new InputStreamReader(is, "UTF-8");
		StringBuilder sb = new StringBuilder();
		int length;
		while ((length = in.read(buf, 0, buf.length)) != -1)
		{
			sb.append(buf, 0, length);
		}
		
		System.out.println(sb.toString());
	}
}
