package demonstrations;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HashMapIterationDemo implements DemonstrationExecutor {
	Scanner input;
	
	public HashMapIterationDemo(Scanner in)
	{
		input = in;
	}
	
	@Override
	public void Execute() {
		System.out.println("Hash Map Iteration Demo");
		System.out.println("How do I iterate through a HashMap?");
		
		HashMap<Integer, String> hm = getDefaultMap();
		
		//just iterate, no modifications:
		printMapKeyValuePairsForEach(hm);
		input.nextLine();
		dashes(80);
		
		printMapKeyValuePairsForEachLambda(hm);
		input.nextLine();
		dashes(80);
		
		printMapKeyValuePairsForEachLambdaOnEntrySet(hm);
		input.nextLine();
		dashes(80);
		
		printMapKeysOnly(hm);
		input.nextLine();
		dashes(80);
		
		printMapValuesOnly(hm);
		input.nextLine();
		dashes(80);
		
		//reset
		hm = getDefaultMap();
		try
		{
			printMapKeyValueWithModError(hm);
		}
		catch(Exception ex)
		{
			ex.printStackTrace(System.out);
		}
		input.nextLine();
		dashes(80);
		
		//reset
		hm = getDefaultMap();
		printMapKeyValueSafeForModification(hm);
		input.nextLine();
		dashes(80);
		
		printMapKeyValuePairsForEachLambda(hm);
		input.nextLine();
		dashes(80);
	}
	
	private HashMap<Integer, String> getDefaultMap()
	{
		HashMap<Integer, String> hm = new HashMap<Integer, String>();
		hm.put(1, "Red");
		hm.put(2, "Orange");
		hm.put(3, "Yellow");
		hm.put(4, "Green");
		hm.put(5, "Blue");
		hm.put(6, "Indigo");
		hm.put(7,  "Violet");
		return hm;
	}
	
	private void dashes(int length)
	{
		System.out.println(Stream.generate(
				() -> String.valueOf('-'))
				.limit(length)
				.collect(Collectors.joining()));
	}
	
	public void printMapKeyValuePairsForEach(HashMap<Integer, String> m)
	{
		System.out.println("Print Map Key Value Pairs with For-Each");
		for (Map.Entry<Integer, String> kvp : m.entrySet())
		{
			System.out.printf("Key: %d\tValue: %s\n"
								, kvp.getKey()
								, kvp.getValue());
		}
	}
	
	public void printMapKeyValuePairsForEachLambda(HashMap<Integer, String> m)
	{
		System.out.println("Print Map Key Value Pairs with ForEach Lambda");
		m.forEach((key, value) -> {
			System.out.printf("Key: %d\tValue: %s\n"
					, key
					, value);
		});
	}
	
	public void printMapKeyValuePairsForEachLambdaOnEntrySet(HashMap<Integer, String> m)
	{
		System.out.println("Print Map Key Value Pairs with ForEach Lambda on EntrySet");
		m.entrySet().forEach(entry -> {
			System.out.printf("Key: %d\tValue: %s\n"
					, entry.getKey()
					, entry.getValue());
		});	
	}

	public void printMapKeysOnly(HashMap<Integer, String> m)
	{
		System.out.println("Print Map Keys Only");
		for (Integer k : m.keySet())
		{
			System.out.printf("Key: %d\n", k);
		}
	}
	
	public void printMapValuesOnly(HashMap<Integer, String> m)
	{
		System.out.println("Print Map Values Only");
		m.values().forEach(value -> {
			System.out.printf("Value: %s\n", value);
		});
	}
	
	public void printMapKeyValueWithModError(HashMap<Integer, String> m)
	{
		System.out.println("Print Map Key Values but have Modification Error");
		int i = 1;
		for (Map.Entry<Integer, String> kvp : m.entrySet())
		{
			System.out.printf("Key: %d\tValue: %s\n"
								, kvp.getKey()
								, kvp.getValue());
			if (i == 4)
			{
				//remove the next item that would have been iterated:
				m.remove(5,"Blue");
			}
			i++;
		}
	}
	
	public void printMapKeyValueSafeForModification(HashMap<Integer, String> m)
	{
		System.out.println("Print Map Key Values but have safe for modification due to Iterator");
		int i = 1;
		Iterator<Map.Entry<Integer, String>> itr = m.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Map.Entry<Integer, String> kvp = itr.next();
			System.out.printf("Key: %d\tValue: %s\n"
					, kvp.getKey()
					, kvp.getValue());
			if (i == 4)
			{
				itr.remove(); //removes "Green"
			}
			i++;
		}
	}
}
