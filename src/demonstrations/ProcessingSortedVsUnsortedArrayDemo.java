package demonstrations;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class ProcessingSortedVsUnsortedArrayDemo implements DemonstrationExecutor {
	Scanner input;
	private final int THRESHOLD = 250000;
	private final int MAX_RANDOM = 500000;
	private final int ARRAY_SIZE = Integer.MAX_VALUE / 25;
	public ProcessingSortedVsUnsortedArrayDemo(Scanner in)
	{
		input = in;
	}
	
	@Override
	public void Execute() {
		System.out.println("Processing Sorted vs. Unsorted Arrays Demonstration");
		System.out.println("Why is processing a sorted array faster than an unsorted array?");
		input.nextLine();
		
		//get two arrays [exec time here is irrelevant]
		int[] unsorted = createTestArray();
		int[] sorted = createTestArray();
		Arrays.sort(sorted);  //as is this: although it will take time...
		
		long startSumUnsorted = System.nanoTime();
		long sumUnsorted = getSum(unsorted);
		long endSumUnsorted = System.nanoTime();
		
		long startSumSorted = System.nanoTime();
		long sumSorted = getSum(sorted);
		long endSumSorted = System.nanoTime();
		
		//without if:
		System.out.println("No if condition in the loops");
		double runtime = calculateRuntimeSeconds(startSumUnsorted, endSumUnsorted);
		System.out.printf("Unsorted runtime in seconds: \t%.9f\n"
								, runtime);
		
		runtime = calculateRuntimeSeconds(startSumSorted, endSumSorted);
		System.out.printf("Sorted runtime in seconds: \t%.9f\n"
								, runtime);
		
		input.nextLine();
		
		
		//with if:
		startSumUnsorted = System.nanoTime();
		long sumUnsortedIf = getSumIfCondition(unsorted);
		endSumUnsorted = System.nanoTime();
		
		startSumSorted = System.nanoTime();
		long sumSortedIf = getSumIfCondition(sorted);
		endSumSorted = System.nanoTime();
		
		System.out.println("With if condition in the loops");
		runtime = calculateRuntimeSeconds(startSumUnsorted, endSumUnsorted);
		System.out.printf("Unsorted runtime in seconds: \t%.9f\n"
								, runtime);
		
		runtime = calculateRuntimeSeconds(startSumSorted, endSumSorted);
		System.out.printf("Sorted runtime in seconds: \t%.9f\n"
								, runtime);
	}
	
	private long getSum(int[] values)
	{
		long sum = 0;
		for (int x : values)
		{
			sum += x;
		}
		return sum;
	}
	
	private long getSumIfCondition(int[] values)
	{
		long sum = 0;
		for (int x : values)
		{
			if (x >= THRESHOLD)
			{
				sum += x;
			}
		}
		return sum;
	}

	private int[] createTestArray()
	{
		int[] retArr = new int[ARRAY_SIZE];
		Random r = new Random();
		
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			retArr[i] = r.nextInt(MAX_RANDOM);
		}
		
		return retArr;
	}
	
	private double calculateRuntimeSeconds(long start, long end)
	{
		return (double)(end - start) / 1E9;
	}
}
