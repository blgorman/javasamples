package demonstrations;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class RandomIntegerGenerationDemo implements DemonstrationExecutor {
	Scanner input;
	public RandomIntegerGenerationDemo(Scanner in)
	{
		input = in;
	}
	
	@Override
	public void Execute() {
		System.out.println("Random Integer Generation");
		System.out.println("Question: How do I generate a random int value within a range (i.e. 5-10)?");
		
		System.out.println("Please enter the minumum number for the range [inclusive]: ");
		int min = input.nextInt();
		input.nextLine();
		System.out.println("Please enter the maximum number for the range [inclusive]: ");
		int max = input.nextInt();
		input.nextLine();
		
		//make sure no overflow or bad data
		if (max == Integer.MAX_VALUE)
		{
			max -= 1;
		}

		GenerateRandomWithJavaUtilRandom(min, max);
		GenerateRandomWithJavaUtilRandomAndSeed(min, max);
		GenerateRandomWithJavaUtilRandomAndRandomSeed(min, max);
		GenerateRandomWithMathRandom(min, max);
		GenerateRandomViaThreadLocalRandom(min, max);
	}
	
	public void GenerateRandomWithJavaUtilRandom(int min, int max)
	{
		Random r = new Random();
		int result = r.nextInt((max - min) + 1) + min;
		System.out.println("Util Random Result: " + result);
	}
	
	public void GenerateRandomWithJavaUtilRandomAndSeed(int min, int max)
	{
		int seed = 10000;
		Random r = new Random(seed);
		int result = r.nextInt((max - min) + 1) + min;
		System.out.println("Util Random Result with Seed: " + result);
	}
	
	public void GenerateRandomWithJavaUtilRandomAndRandomSeed(int min, int max)
	{
		Random r = new Random();
		int seed = r.nextInt();
		Random r2 = new Random(seed);
		int result = r2.nextInt((max - min) + 1) + min;
		System.out.println("Util Random Result with Random Seed: " + result);
	}
	
	
	public void GenerateRandomWithMathRandom(int min, int max)
	{
		double result = Math.random(); //returns a double from 0.0 to 1.0
		System.out.println("Math Random result: " + result);
		
		//figure out the percent value based on the random
		long percent = Math.round(100.0 * result);  
		//figure out the range [both ends inclusive]
		int range = max - min;
		//what value in the range best represents the percent?
		double x = percent * range / 100.0 + min;
		long newResult = Math.round(x);
		System.out.println("Adjusted Math Random: " + newResult);
	}
	
	public void GenerateRandomViaThreadLocalRandom(int min, int max)
	{
		int result = ThreadLocalRandom.current().nextInt(min, max + 1);
		System.out.println("ThreadLocalRandom result: " + result);
	}

}
