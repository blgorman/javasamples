import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import demonstrations.*;

public class DemonstrationProgram {
	public static Scanner input;
	
	public static void main(String[] args) {
		input = new Scanner(System.in);
		
		boolean exit = false;
		while (!exit)
		{
			int choice = displayMenu();
			
			DemonstrationExecutor dex = getDemonstration(input, choice);
			dex.Execute();
			
			System.out.println("Demonstration Complete.  Would you "
					+ "like to do another demonstration [y/n]?");
			String doAnotherDemo = input.nextLine();
			if (doAnotherDemo.toLowerCase().charAt(0) == 'n')
			{
				exit = true;
			}
		}
		
		System.out.println("Demonstration Complete.");
		System.out.println("Thank you and have a great day! ");
	}

	private static List<MenuOption> getOptions()
	{
		List<MenuOption> options = new ArrayList<MenuOption>();
		
		options.add(new MenuOption("Generate Random Integers", 1));
		options.add(new MenuOption("Iterate through a HashMap", 2));
		options.add(new MenuOption("InputStream to String", 3));
		options.add(new MenuOption("Java Pass-By-Reference/Value", 4));
		options.add(new MenuOption("Processing Sorted vs. unsorted Arrays", 5));
		options.add(new MenuOption("Subtracting Dates", 6));
		return options;
	}
	
	private static DemonstrationExecutor getDemonstration(Scanner input, int choice)
	{
		switch (choice)
		{
			case 1: 
				return new RandomIntegerGenerationDemo(input);
			case 2:
				return new HashMapIterationDemo(input);
			case 3:
				return new InputStreamToStringDemo(input);
			case 4:
				return new JavaPassByReferenceValueDemo(input);
			case 5:
				return new ProcessingSortedVsUnsortedArrayDemo(input);
			case 6:
				return new SubtractingDatesDemo(input);
			default:
				return new BadChoice();
		}
	}
	
	private static int displayMenu()
	{
		System.out.println(stars(80));
		System.out.println("Please select your choice: ");
		List<MenuOption> options = getOptions();
		options.forEach(x -> System.out.println(x.toString()));
		System.out.println(stars(80));
		int result = Integer.parseInt(input.nextLine());
		return result;
	}
	
	private static String stars(int length)
	{
		return Stream.generate(
						() -> String.valueOf('*'))
						.limit(length)
						.collect(Collectors.joining());
	}
}
