package CustomObjects;

public class Car
{
	private String make;
	private String model;
	private int year;
	private String color;
	
	public Car()
	{
	}
	
	public Car(String theMake, String theModel, int theYear, String theColor)
	{
		make = theMake;
		model = theModel;
		year = theYear;
		color = theColor;
	}
	
	public String getMake()
	{
		return make;
	}
	
	public void setMake(String theMake)
	{
		make = theMake;
	}
	
	public String getModel()
	{
		return model;
	}
	
	public void setModel(String theModel)
	{
		model = theModel;
	}
	
	public String getColor()
	{
		return color;
	}
	
	public void setColor(String theColor)
	{
		color = theColor;
	}
	
	public int getYear()
	{
		return year;
	}
	
	public void setYear(int theYear)
	{
		year = theYear;
	}
	
	public String toString()
	{
		return String.format("%d %s %s %s"
								, getYear()
								, getColor()
								, getMake()
								, getModel());
	}
}
