//this is the menu option
public class MenuOption {
	private String displayText;
	private int optionNumber;
	
	public MenuOption(String text, int option)
	{
		displayText = text;
		optionNumber = option;
	}
	
	public String toString()
	{
		return String.format("%d] %s"
							, optionNumber
							, displayText);
	}
}
